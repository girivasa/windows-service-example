﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;


namespace MyWindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        //private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main()
        {
            //Logger.Debug("Starting Windows Example Service");
            
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Service1() 
            };
            ServiceBase.Run(ServicesToRun);
        }

       
    }
}
