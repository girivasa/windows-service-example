using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.X509;
using SslCertBinding.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Win32;
using System.Security.Permissions;

namespace PCWixCustomAction
{
    public class Win32
    {
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern uint RegisterWindowMessage(string lpString);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendNotifyMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        public static IntPtr HWND_BROADCAST = (IntPtr)0xffff;

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);
    }

    public class CustomActions
    {
        public static void sendBroadcastMessage(string msgName)
        {
            uint msg = Win32.RegisterWindowMessage(msgName);

            Win32.SendNotifyMessage(Win32.HWND_BROADCAST, msg, (IntPtr)0, (IntPtr)0);
        }

        [CustomAction]
        public static ActionResult SendBroadcastMessage(Session session)
        {
            session.Log("Begin SendBroadcastMessage");
            string windowMessageName = session["WindowMessageName"];
            if (String.IsNullOrEmpty(windowMessageName))
            {
                session.Log("No window message name found");
                return ActionResult.Failure;
            }

            session.Log("Sending broadcast message: " + windowMessageName);
            sendBroadcastMessage(windowMessageName);
            session.Log("Completed send Message");
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult DetectPCRunning(Session session)
        {
            Process[] pcProcesses = Process.GetProcessesByName("PureCloud");
            Process[] pcdProcesses = Process.GetProcessesByName("PureCloud Directory");
            Process[] gcProcesses = Process.GetProcessesByName("GenesysCloud");
            if (pcProcesses.Length > 0 || pcdProcesses.Length > 0 || gcProcesses.Length > 0)
            {
                session.Log("GenesysCloud process found to be running. Will launch...");
                session["PCRUNNING"] = "1";
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult RefreshIconCache(Session session)
        {
            session.Log("Entering RefreshIconCache()...");

            Process pRefreshIconCache = new Process();
            pRefreshIconCache.StartInfo.FileName = "ie4uinit.exe";
            pRefreshIconCache.StartInfo.UseShellExecute = false;
            pRefreshIconCache.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
            pRefreshIconCache.StartInfo.CreateNoWindow = true;
            if (Convert.ToInt32(Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion").GetValue("CurrentMajorVersionNumber")) >= 10)
            {
                session.Log("Running on Windows 10 or later...");
                pRefreshIconCache.StartInfo.Arguments = "-show";
            }
            else
            {
                session.Log("Running on a version prior to Windows 10...");
                pRefreshIconCache.StartInfo.Arguments = "-ClearIconCache";
            }

            IntPtr ptr = new IntPtr();
            try
            {
                session.Log("Beginning the ie4uinit.exe process to refresh the icon cache...");

                Win32.Wow64DisableWow64FsRedirection(ref ptr);

                pRefreshIconCache.Start();
                pRefreshIconCache.WaitForExit();
                pRefreshIconCache.Close();

                Win32.Wow64RevertWow64FsRedirection(ptr);
            }
            catch (Exception e)
            {
                session.Log("The ie4uinit.exe encountered an exception... " + e.ToString());
                Win32.Wow64RevertWow64FsRedirection(ptr);
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult InstallNewGenesysCloudCertificates(Session session)
        {
            session.Log("Inspecting certificate store for Genesys CA...");
            GenesysSslCertificateManager.SetupCertificates(session);
            return ActionResult.Success;
        }
    }

    public class GenesysSslCertificateManager
    {
        const String certificateAuthorityGenesys = "LocalTestGenesysCA";
        const String certificateAuthorityLocalhost = "localhost";
        static Session session = null;

        public static void SetupCertificates(Session parentSession)
        {
            session = parentSession;
            try
            {
                if (CertificateExists(certificateAuthorityGenesys, StoreName.Root, StoreLocation.LocalMachine, null) &&
                    CertificateExists(certificateAuthorityLocalhost, StoreName.My, StoreLocation.LocalMachine, certificateAuthorityGenesys))
                {
                    session.Log("Both certificates already found - not going to create them.");
                    return;
                }
                AsymmetricKeyParameter caPrivateKey = null;
                var caCert = GenerateCACertificate("CN=" + certificateAuthorityGenesys, ref caPrivateKey);
                addCertToStore(caCert, StoreName.Root, StoreLocation.LocalMachine, "Root store");

                var clientCert = GenerateSelfSignedCertificate("CN=" + certificateAuthorityLocalhost, "CN=" + certificateAuthorityGenesys, caPrivateKey);
                var p12 = clientCert.Export(X509ContentType.Pfx);

                addCertToStore(new X509Certificate2(p12, (string)null, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet),
                    StoreName.My, StoreLocation.LocalMachine, "Personal store");
                BindCertificateToPort(clientCert.Thumbprint);
            }
            catch (Exception e)
            {
                session.Log($"Received an error while trying to create the certificate: {e.Message}\n{e.StackTrace}");
            }
        }

        public static Boolean CertificateExists(string subjectName, StoreName storeName, StoreLocation storeLocation, String issuerName)
        {
            X509Store store = new X509Store(storeName, storeLocation);

            store.Open(OpenFlags.ReadOnly);
            // Set 'validOnly' to false for the time being
            var certificates = store.Certificates.Find(X509FindType.FindBySubjectName, subjectName, false);
            if (issuerName != null)
            {
                certificates = certificates.Find(X509FindType.FindByIssuerName, issuerName, false);
            }
            store.Close();
            return certificates != null && certificates.Count > 0;
        }

        public static X509Certificate2 GenerateSelfSignedCertificate(string subjectName, string issuerName, AsymmetricKeyParameter issuerPrivKey)
        {
            session.Log($"Generating Client Certificate for {subjectName}");
            const int keyStrength = 2048;

            // Generating Random Numbers
            CryptoApiRandomGenerator randomGenerator = new CryptoApiRandomGenerator();
            SecureRandom random = new SecureRandom(randomGenerator);

            // The Certificate Generator
            X509V3CertificateGenerator certificateGenerator = new X509V3CertificateGenerator();

            // Serial Number
            BigInteger serialNumber = BigIntegers.CreateRandomInRange(BigInteger.One, BigInteger.ValueOf(Int64.MaxValue), random);
            certificateGenerator.SetSerialNumber(serialNumber);

            // Signature Algorithm
            const string signatureAlgorithm = "SHA256WithRSA";
            certificateGenerator.SetSignatureAlgorithm(signatureAlgorithm);

            // Issuer and Subject Name
            X509Name subjectDN = new X509Name(subjectName);
            X509Name issuerDN = new X509Name(issuerName);
            certificateGenerator.SetIssuerDN(issuerDN);
            certificateGenerator.SetSubjectDN(subjectDN);

            // Valid For
            DateTime notBefore = DateTime.UtcNow.Date;
            DateTime notAfter = notBefore.AddYears(2);

            certificateGenerator.SetNotBefore(notBefore);
            certificateGenerator.SetNotAfter(notAfter);

            // Subject Public Key
            AsymmetricCipherKeyPair subjectKeyPair;
            var keyGenerationParameters = new KeyGenerationParameters(random, keyStrength);
            var keyPairGenerator = new RsaKeyPairGenerator();
            keyPairGenerator.Init(keyGenerationParameters);
            subjectKeyPair = keyPairGenerator.GenerateKeyPair();

            certificateGenerator.SetPublicKey(subjectKeyPair.Public);

            // Generating the Certificate
            AsymmetricCipherKeyPair issuerKeyPair = subjectKeyPair;

            // selfsign certificate
            Org.BouncyCastle.X509.X509Certificate certificate = certificateGenerator.Generate(issuerPrivKey, random);


            // correcponding private key
            PrivateKeyInfo info = PrivateKeyInfoFactory.CreatePrivateKeyInfo(subjectKeyPair.Private);


            // merge into X509Certificate2
            X509Certificate2 x509 = new System.Security.Cryptography.X509Certificates.X509Certificate2(certificate.GetEncoded());

            Asn1Sequence seq = (Asn1Sequence)Asn1Object.FromByteArray(info.ParsePrivateKey().GetDerEncoded());
            if (seq.Count != 9)
            {
                //throw new PemException("malformed sequence in RSA private key");
            }

            RsaPrivateKeyStructure rsa = new RsaPrivateKeyStructure(seq);
            RsaPrivateCrtKeyParameters rsaparams = new RsaPrivateCrtKeyParameters(
                rsa.Modulus, rsa.PublicExponent, rsa.PrivateExponent, rsa.Prime1, rsa.Prime2, rsa.Exponent1, rsa.Exponent2, rsa.Coefficient);

            x509.PrivateKey = ToDotNetKey(rsaparams); //x509.PrivateKey = DotNetUtilities.ToRSA(rsaparams);
            return x509;

        }

        public static AsymmetricAlgorithm ToDotNetKey(RsaPrivateCrtKeyParameters privateKey)
        {
            var cspParams = new CspParameters
            {
                KeyContainerName = Guid.NewGuid().ToString(),
                KeyNumber = (int)KeyNumber.Exchange,
                Flags = CspProviderFlags.UseMachineKeyStore
            };

            var rsaProvider = new RSACryptoServiceProvider(cspParams);
            var parameters = new RSAParameters
            {
                Modulus = privateKey.Modulus.ToByteArrayUnsigned(),
                P = privateKey.P.ToByteArrayUnsigned(),
                Q = privateKey.Q.ToByteArrayUnsigned(),
                DP = privateKey.DP.ToByteArrayUnsigned(),
                DQ = privateKey.DQ.ToByteArrayUnsigned(),
                InverseQ = privateKey.QInv.ToByteArrayUnsigned(),
                D = privateKey.Exponent.ToByteArrayUnsigned(),
                Exponent = privateKey.PublicExponent.ToByteArrayUnsigned()
            };

            rsaProvider.ImportParameters(parameters);
            return rsaProvider;
        }

        public static X509Certificate2 GenerateCACertificate(string subjectName, ref AsymmetricKeyParameter CaPrivateKey)
        {
            session.Log($"Generating CA Cert for {subjectName}");
            const int keyStrength = 2048;

            // Generating Random Numbers
            CryptoApiRandomGenerator randomGenerator = new CryptoApiRandomGenerator();
            SecureRandom random = new SecureRandom(randomGenerator);

            // The Certificate Generator
            X509V3CertificateGenerator certificateGenerator = new X509V3CertificateGenerator();

            // Serial Number
            BigInteger serialNumber = BigIntegers.CreateRandomInRange(BigInteger.One, BigInteger.ValueOf(Int64.MaxValue), random);
            certificateGenerator.SetSerialNumber(serialNumber);

            // Signature Algorithm
            const string signatureAlgorithm = "SHA256WithRSA";
            certificateGenerator.SetSignatureAlgorithm(signatureAlgorithm);

            // Issuer and Subject Name
            X509Name subjectDN = new X509Name(subjectName);
            X509Name issuerDN = subjectDN;
            certificateGenerator.SetIssuerDN(issuerDN);
            certificateGenerator.SetSubjectDN(subjectDN);

            // Valid For
            DateTime notBefore = DateTime.UtcNow.Date;
            DateTime notAfter = notBefore.AddYears(2);

            certificateGenerator.SetNotBefore(notBefore);
            certificateGenerator.SetNotAfter(notAfter);

            // Subject Public Key
            AsymmetricCipherKeyPair subjectKeyPair;
            KeyGenerationParameters keyGenerationParameters = new KeyGenerationParameters(random, keyStrength);
            RsaKeyPairGenerator keyPairGenerator = new RsaKeyPairGenerator();
            keyPairGenerator.Init(keyGenerationParameters);
            subjectKeyPair = keyPairGenerator.GenerateKeyPair();

            certificateGenerator.SetPublicKey(subjectKeyPair.Public);

            // Generating the Certificate
            AsymmetricCipherKeyPair issuerKeyPair = subjectKeyPair;

            // selfsign certificate
            Org.BouncyCastle.X509.X509Certificate certificate = certificateGenerator.Generate(issuerKeyPair.Private, random);
            X509Certificate2 x509 = new X509Certificate2(certificate.GetEncoded());

            CaPrivateKey = issuerKeyPair.Private;

            return x509;
            //return issuerKeyPair.Private;
        }

        public static bool addCertToStore(System.Security.Cryptography.X509Certificates.X509Certificate2 cert, System.Security.Cryptography.X509Certificates.StoreName st, System.Security.Cryptography.X509Certificates.StoreLocation sl, String sName)
        {
            session.Log("Adding certificate to store "+ sName);
            bool bRet = false;

            try
            {
                X509Store store = new X509Store(st, sl);
                //StorePermission sp = new StorePermission(PermissionState.Unrestricted);
               // sp.Flags = StorePermissionFlags.OpenStore;
                //sp.Assert();
                //store.Open(OpenFlags.MaxAllowed);
                store.Open(OpenFlags.ReadWrite);
                store.Add(cert);

                store.Close();
            }
            catch (Exception e)
            {
                session.Log($"Received an error while trying to store the certificate: {e.Message}\n{e.StackTrace}");
            }

            return bRet;
        }

        static void BindCertificateToPort(String thumbprint)
        {
            ICertificateBindingConfiguration config = new CertificateBindingConfiguration();
            var ipPort = new IPEndPoint(IPAddress.Parse("0.0.0.0"), 10103);
            var certificateThumbprint = thumbprint;
            var appId = Guid.Parse("cbd14fd4-8671-4a92-b06b-9dc21268784c");

            // add a new binding record
            config.Bind(new CertificateBinding(certificateThumbprint, StoreName.My, ipPort, appId));
        }
    }
}
